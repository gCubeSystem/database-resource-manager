
# Changelog


## [v1.4.1] [r4.24.0] - 2020-06-10

### Features

- Updated for support https protocol [#19423]

## [v1.3.0] - 2014-12-05

### Features

- Method submitQuery modified in class DatabaseManagement in order to return as output a file and a map. The map is truncated to 1000 rows if the result has more than 1000 rows. method writeTableIntoFile reintroduced that performs the file writing and the map generation.
- Method submitQuery modified in class DatabaseManagement adding the UUID string to the submitQuery result file QueryResult file
- Samplings and submitQuery changed in order to return as a result a map with column values truncated to 255 characters and a file with the whole columns values in order to manage so to manage big data properly and the conversion of a geometry data type in a multipolygon representation applying the function st_astext() for a database postgres.
- WriteSubmitResultIntoFile method modified removing special characters using regular expressions in order to write data in a file correctly.
- Total number rows returned as output for the result of the submit Query operation:class DatabaseManagement modified

## [v1.2.0] - 2014-10-27

### Features

- Class DBResource modified in order to remove for a resource duplicated access points elements that have the same endpoint, username and password values.
- Method closeconnection in class DatabaseManager modified in order to close the SessionFactory. Sampler class modified.
- Bug fixed and related to samplings operation to manage column uppercase names with a postgres database.
- For the getTableDetails operation column names recovered as defined in their database's schema. PostgresTableStructure class modified.
- Bug fixed in class ConnectionManager for the submitquery operation modified in order to manage the data geometry recovering the related value as a sequence of hexadecimal characters and not as a parsed multipolygon value.
- Bug fixed in class DatabaseManagement for the submitquery operation in order to truncate column values of the result to 255 character.
- Bug fixed in Sampler class in computeSmartSampleWithThreshold method. table name formatted as schema."table" in order to recover the rows.
- Bug fixed for the submitQuery and sampling operation and related to column value truncated to 255 characters. If the final quote has been truncated the parsecvsstring method in the servlet does not work fine. Bug fixed adding the final quote. convertToJavaType method in DatabaseManagement class modified.

## [v1.1.0] - 2014-09-10

### Features

- File DestinationDBHibernate.cfg.xml in "cfg" folder of the project renamed in DestinationDBHibernateDBManager.cfg.xml and correction in method initDBSession of the ConectionMannaager class in order to specify this file name.
- Bug fixed for the smart sampling on table scientific_names of col2oct2010.Table name is overwritten when two iterations are performed to extract 200 rows.Corrections in computeSmartSampleWithThreshold.
- Input parameters updated in the SubmitQuery method of DatabaseManagement class.(input parameter on path file removed because unusable)
- Bug hibernate on join tables with same column names.ConnectionManager and DatabaseManagement classes modified adding the method "executeQueryJDBC" in ConnectionManager, used only for the submitquery operation. The method uses JDBC.
- Bug fixed in sampling operations with an attribute of geometry type that slows the result visualization. Attributes truncated to 255 characters.
- Bug fixed with the rand() function in the smart and random sample for a database mysql with the computeSmartSampleWithThreshold method
- Bug fixed in Sampler and DatabaseOPerations classes to manage case sensitive tables for database Postgres and for GetTableDetails
- Class DatabaseManagement modified in order to not make available the result in a file csv for submitquery and samplings operations
- Added a timeout database connection
- bug fixed in samplings on table names that are keywords.The name considered is schema.table for postgres and database.table for mysql
- Bug fixed in sampler in order to have an attributes header with the same order defined in the database schema
- Bug corrected in "showCreateTable" method of MySQLTableStructure class.Result managed in a correct way using aliasToEntityOrderedMapResultTransformer class

## [v1.0.0] - 2014-06-09

### Features

- First Release


This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).